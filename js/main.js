(function($) {
	'use strict';
	

	jQuery(document).ready(function(){
		
	// one page nav
	$('#nav').onePageNav({
	    currentClass: 'current',
	    changeHash: true,
	    scrollSpeed: 4000,
	    scrollThreshold: 0.5,
	    filter: '',
	    easing: 'swing',
	    begin: function() {
	        //I get fired when the animation is starting
	    },
	    end: function() {
	        //I get fired when the animation is ending
	    },
	    scrollChange: function(jQuerycurrentListItem) {
	        //I get fired when you enter a section and I pass the list item of the section
	    }
	});		
		
	/*PRELOADER JS*/
		$(window).load(function() { 
			$('.status').fadeOut();
			$('.preloader').delay(350).fadeOut('slow'); 
		}); 
		/*END PRELOADER JS*/

		// jQuery Lightbox
		$('.lightbox').venobox({
			numeratio: true,
			infinigall: true
		});	
		
		
		/*START MENU JS*/
		$('a').on('click', function(e){
			var anchor = $(this);
			$('html, body').stop().animate({
				scrollTop: $(anchor.attr('href')).offset().top - 50
			}, 1500);
			e.preventDefault();
		});		
			
		/*START MIXITUP JS*/
		
			$('.port_items').mixItUp();	
		
		//for scrolltop
		
		$(window).scroll(function () {
			if ($(this).scrollTop() > 200) {
				$('.scrollup').fadeIn();
			} else {
				$('.scrollup').fadeOut();
			}
		});
		
		$('.scrollup').click(function () {
			$("html, body").animate({
				scrollTop:0
			},600);
			return false;
		});		


		$(".partner-images").owlCarousel({
			autoplay:true,
			items: 8,
			margin: 80,
			responsive:{
            0:{
                items:2
            },
            400:{
                items:3
            },
            600:{
                items:5
            },
            992:{
                items:6
            }
        }
		
		});
		
		$('.counter').counterUp({
			delay: 10,
			time: 1000
		});
		
	}); 
	
	

	/*  Stellar for background scrolling  */
	(function () {

		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		 
		} else {
			$(window).stellar({
				horizontalScrolling: false,
				responsive: true
			});
		}

	}());
	/* End Stellar for background scrolling  */		
	
	
	/* Youtube Video  */		
	
	$('.player').mb_YTPlayer();
	
	/*START WOW ANIMATION JS*/
	  new WOW().init();	
	/*END WOW ANIMATION JS*/	
	
})(jQuery);	